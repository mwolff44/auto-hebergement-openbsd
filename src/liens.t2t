Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Liens et références==
===Références===

Ce document n'est pas le fruit de mon invention, mais rassemble des
informations glanées ci et là.

- FAQ d'OpenBSD : http://www.openbsd.org/faq/index.html
- FAQ sur pf : http://www.openbsd.org/faq/pf/filter.html
- FAQ d'Opensmtpd : https://www.opensmtpd.org/faq/index.html
- Absolute OpenBSD : http://blather.michaelwlucas.com/archives/846
- Installation détaillée d'un serveur mail :

http://technoquarter.blogspot.fr

- Notes de Vigdis : https://chown.me

- Configuration de spamd : 

https://www.pantz.org/software/spamd/configspamd.html

- Configuration de symon : 

https://dummyobsd.blogspot.fr/2013/12/openbsd-basic-configuration-symon.html

- Configuration d'owncloud (nextcloud) : 

https://github.com/reyk/httpd/wiki/Running-ownCloud-with-httpd-on-OpenBSD
- Wiki auto-hébergement : http://wiki.auto-hebergement.fr/
- Un annuaire d’applications et de services à auto-héberger : 

http://waah.quent1.fr/doku.php
- Des applications simples à auto-héberger sélectionnées par sebsauvage : 

http://sebsauvage.net/auto/

- Documentation PHP : 

https://secure.php.net/manual/en/install.unix.openbsd.php
- De nombreux autres forums et blogs perdus dans l'oubli...



===Liens===
Voici plusieurs liens énoncés au cours de ce document, plus ou moins dans
leur ordre d'apparition : 

- Site d'OpenBSD : http://www.openbsd.org/
- Conférence "Je n'ai rien à cacher" : http://jenairienacacher.fr
- Documentation officielle pour l'installation d'OpenBSD :

  http://www.openbsd.org/faq/faq4.html
- Miroirs de téléchargement d'OpenBSD : 

http://www.openbsd.org/ftp.html
- Logiciel Rufus : https://rufus.akeo.ie/
- Manuel de disklabel : 

http://man.openbsd.org/OpenBSD-current/man8/disklabel.8
- OpenSSH : http://www.openssh.com/
- SQlite : https://www.sqlite.org/
- Client de messagerie Thunderbird : 

https://www.mozilla.org/fr/thunderbird/
- Tester si ses messages sont des spams : https://www.mail-tester.com/
- Documentation sur la mise en place d'une solution antispan avec bgpd :
  http://bgp-spamd.net/client/
- Site officiel du projet tor : https://www.torproject.org/
- Navigateur Torbrowser : 

https://www.torproject.org/projects/torbrowser.html.en
- Extension Firefox pour naviguer sur les site gopher :

  http://gopher.floodgap.com/overbite/
- Page errata d'OpenBSD pour connaître les patches de sécurité disponibles :
  http://www.openbsd.org/errata.html
- M:Tier : http://www.mtier.org/
- Webmail roundcube : https://roundcube.net/
- Nextcloud : https://nextcloud.com/  --

  https://docs.nextcloud.com/server/10/admin_manual/

- BoZon et son adresse de téléchargement : http://bozon.pw/fr --

  https://github.com/broncowdd/BoZoN/archive/master.zip

- Blogotext et ses captures d'écran :

  http://lehollandaisvolant.net/blogotext/fr/ --

  http://lehollandaisvolant.net/blogotext/fr/?screenshots

- Wordpress : https://wordpress.org/
- Baïkal et son adresse de téléchargement : http://sabre.io/baikal/ --

  https://github.com/fruux/Baikal/releases

- Extension pour Thunderbird de gestion de calendrier :

  https://www.mozilla.org/en-US/projects/calendar/

  et

  https://www.guillaume-leduc.fr/synchronisez-baikal-avec-thunderbird-et-lightning.html

- Exemple de configuration de Thunderbird avec Baïkal :

  https://www.mozilla.org/en-US/projects/calendar/

- Site de Dokuwiki : https://www.dokuwiki.org/fr:dokuwiki
- Site de Webalizer : http://www.patrickfrei.ch/webalizer/
- Site de Piwik : https://piwik.org/
- Documentation officielle de piwik concernant son installation :

  https://piwik.org/docs/requirements/

- Certificats SSL avec Letsencrypt : https://letsencrypt.org/
- Documentation officielle de letsencrypt :
  https://letsencrypt.org/getting-started/
- Tester la qualité du chiffrement de votre site avec ssllabs :

  https://www.ssllabs.com/
- FDN : http://www.fdn.fr/
- Liste de diffusion d'OpenBSD : https://www.openbsd.org/mail.html
- Forum et wiki francophone OBSD4* : http://obsd4a.net/qa/ - 
  http://obsd4a.net/wiki
- Documentation sur l'utilisation de vi :

  http://wiki.linux-france.org/wiki/Utilisation_de_vi

- Wiki des applications auto-hébergées : http://waah.quent1.fr/
- Sécuriser son shell SSH : 
  https://www.guillaume-leduc.fr/securiser-secure-shell-ssh.html
- Contrôle tes données : https://controle-tes-donnees.net/
- Divers tutoriels en anglais pour OpenBSD : http://2f30.org/guides.html
- Mise en place d'OpenVPN : http://openbsdsupport.org/openvpn-on-openbsd.html
- Configuration de relayd :

  http://www.reykfloeter.com/post/41814177050/relayd-ssl-interception

- Documentation ffmpeg : 
    https://trac.ffmpeg.org/wiki/Capture/Webcam

    https://trac.ffmpeg.org/wiki/StreamingGuide

- KrISS feeds : http://tontof.net/kriss/feed/
- Documentation de Tiny Tiny RSS :
  https://tt-rss.org/gitlab/fox/tt-rss/wikis/InstallationNotes
- DNS et vie privée : https://blog.imirhil.fr/2015/02/18/vie-privee-dns.html
- Documentation NSD (anglais) :
  https://www.nlnetlabs.nl/publications/dnssec_howto/
- Génération de clés pour SSH :
https://blog.stephane-huc.net/?post/SSH-Configuration-et-cl%C3%A9s-plus-s%C3%A9curis%C3%A9es

- Explications sur DNSSEC : https://www.22decembre.eu/2017/02/19/dnssec-fr/
