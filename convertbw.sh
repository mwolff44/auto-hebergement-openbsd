#!/bin/sh
# Auteur :      thuban <thuban@yeuxdelibad.net>
# licence :     GNU General Public Licence v3

# Description :
# Depends 

for i in *.jpg; do
    if [ -z "$(echo $i |grep 'gray')" ]; then
        echo $i
        convert -colorspace gray -average $i "${i%.*}-gray.jpg"
    fi
done

for i in *.png; do
    if [ -z "$(echo $i |grep 'gray')" ]; then
        echo $i
        convert -colorspace gray -average $i "${i%.*}-gray.png"
    fi
done
