Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4



==Pré-requis==

Dans ce document, on supposera que : 

- Le système sur votre serveur sera OpenBSD en version OBSDVERSION. Quelques conseils sont proposés
  pour l'installation [dans la suite du document #install].
- Utiliser la ligne de commande ne vous fait plus peur, tant qu'on vous explique
  ce qu'il faut écrire.
- Vous êtes capable d'éditer un fichier.
- Vous serez attentif à différencier les commandes à taper dans un terminal des
  lignes de configuration à écrire dans un fichier. 
- Gardez en tête que les lignes
  de code trop longues sont automatiquement découpées, précédées d'un alinéa. On
  les distingue facilement car lors d'une césure, le symbole ``\`` est
  ajouté dans la version papier du présent document.
- Vous disposez d'un vieil ordinateur récupéré qui servira de
  serveur, ou bien d'un appareil acheté pour l'occasion (basse
  consommation, configuration adaptée à vos besoins particuliers...). 
  Puisqu'OpenBSD supporte de nombreuses architectures, 
  choisissez ``i386``, sinon ``amd64`` pour du matériel plus récent. Dans le doute,
  ça fonctionnera avec ``i386``.
- Vous devez connaître l'adresse [IP #IP] de votre serveur.
- Vous disposez d'un nom de domaine. Si vous ignorez ce que c'est,
  référez vous à la [partie consacrée à ce sujet #NDD].


===Quelle est mon adresse IP ?===[IP]

Avant de définir une 
[adresse IP https://fr.wikipedia.org/wiki/Adresse_IP] (Internet Protocol),
soyons sérieux deux minutes pour parler du Père Noël. Chaque enfant sait très
bien qu'il doit envoyer sa liste de cadeaux à "Route du Ciel, Pôle Nord". S'il veut une
réponse, il notera sa propre adresse derrière l'enveloppe.

C'est le même principe pour l'ordinateur de votre collègue lourd qui vous montre
une vidéo de chat sur Youtube : l'ordinateur sait où trouver Youtube, et Youtube sait à quel adresse envoyer les données.

Chaque appareil connecté à Internet dispose de son adresse : l'adresse IP. 
Cependant, les ordinateurs n'ont pas
le même sens de la poésie que les enfants. Les adresses utilisées dans un réseau
sont des séries de chiffres, par exemple &IPV4. Nettement moins classe que
l'adresse de Père Noël.

	D'accord, mais comment font les serveurs pour connaître l'adresse d'un
	site? Il y a un annuaire ?

Quelle idée intéressante. C'est effectivement prévu, lisez la partie sur les 
[noms de domaine #NDD] pour en savoir davantage.

Il faut distinguer l'IP publique de l'IP locale. La plupart du temps, pour vous
connecter à Internet, votre fournisseur d'accès vous a donné une *box. Celle-ci
fait le relai entre internet et votre ordinateur. Cela donne : 

```
Ordinateur ---->---- *Box ---->---- "Serveur (Internet)"
IP locale         IP publique      IP publique du serveur
```


S'il faut retenir quelque chose, c'est : 
- L'IP locale du serveur servira pour les communications qui restent à
  l'intérieur de votre réseau privé : entre les machines connectées à votre
  *box.
- L'IP publique servira dès que des données seront à échanger sur l'Internet
  mondial.



Pour connaître votre IP publique, il existe de nombreux services en ligne, par exemple sur [mon-ip.com http://www.mon-ip.com], [lehollandaivolant.net http://lehollandaisvolant.net/tout/ip], 
[who.is http://who.is/] , [ipecho http://ipecho.net/] ou encore
[whatsmyip http://www.whatsmyip.org/]. Ça devrait suffire.


Pour trouver l'IP locale de votre serveur, saisissez la commande suivante : 

```
# /sbin/ifconfig |grep inet
```

Vous verrez apparaître des lignes comme celles-ci : 

```
	inet6 ::1 prefixlen 128
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x3
	inet 127.0.0.1 netmask 0xff000000
	inet $LOCIPV4 netmask 0xffffff00 broadcast 192.168.1.255
	inet6 fe80::feaa:14ff:fe65:5f86%re0 prefixlen 64 scopeid 0x1
```

Les adresses ``inet6`` sont des IPv6 et ``inet`` des IPv4.


- ``::1`` et ``127.0.0.1`` : Il s'agit des adresses "internes" de votre serveur, ce n'est pas ce
  que nous cherchons.
- Les adresses commençant par ``fe80:`` et ``192.168`` sont les IP locales de
  votre serveur.
  

Si quelqu'un veut visiter votre site web, il ne connaît que votre IP publique.
Or, c'est la *box qui a l'IP publique, les données ne sont pas dessus mais sur
votre serveur.
Elle doit donc savoir qu'elle
doit diriger ce visiteur vers l'IP locale de votre serveur. La *Box est un
routeur qu'il faut configurer. Ça tombe bien, c'est expliqué 
[juste après #router].


===Rediriger les ports sur son routeur===[router]

Votre serveur, tout comme votre ordinateur actuellement, sera certainement connecté à Internet par l’intermédiaire d’un modem (une *box).
Il faut s’assurer que lorsqu’un visiteur voudra accéder à votre serveur, la
*box le redirige bien vers votre serveur, et non vers une autre machine du
réseau local. On dit que l’on configure le routeur.

Autrement dit, imaginez votre *box comme un grand mur avec dedans plusieurs
portes. Chacune est numérotée.
Quand quelqu’un veut accéder à votre serveur, il va venir frapper à l’une
des portes, par exemple la numéro 80 pour un serveur http. Afin que
tout fonctionne bien, il est nécessaire de savoir où mène la porte numéro 80.
Si la box ne le sait pas, alors la porte reste fermée et votre serveur
est inaccessible.
Bien sûr, pour encore plus de sécurité, une fois la porte 80 passée, votre
serveur sera équipé d’un parefeu pour vérifier que vous avez bien le droit
d’entrer.

[img/schema_routeur.png]

Dans le schéma ci-dessus, seuls les ports 443, 80 et 22 sont associés au
serveur.
Si le petit malin demande un port qui n’est pas redirigé vers le serveur (la
porte est fermée), alors la requête ne peut pas aller jusqu'au bout. C’est
comme s’il demandait d'aller à une destination qui n’existe pas.
En revanche, lorsque le visiteur demande de passer par la porte 80, il est
bien renvoyé vers le serveur.

La configuration du routeur se déroule toujours de la même façon : 

+ Vous accédez à l’interface de configuration du modem ;
+ Vous précisez le port d’écoute par lequel vont arriver les requêtes. Par
exemple, le port 80 pour un site web ;
+ Vous indiquez que le flux qui est adressé à ce port doit être mis en relation
avec le port 80 de votre serveur (et pas un autre ordinateur connecté
à la *box).


Cependant, l’interface de configuration n’est pas la même selon si vous
avez une livebox, freebox, modem OVH... Pas d’inquiétude, on peut trouver
l’adresse à taper dans un navigateur web pour accéder à cette interface.
Essayez dans l’ordre suivant 
(Bien sûr, cette “adresse” est à utiliser sur un ordinateur lui-même connecté
à la *box.) : 

- 192.168.0.1
- 192.168.1.1
- 192.168.1.254
- Pour une freebox, cela se passe en ligne sur la page de gestion de votre compte.



Il est possible qu’un nom d’utilisateur et un mot de passe soient demandés. Essayez dans ce cas admin/admin, sinon, demandez directement à votre
fournisseur d’accès à Internet. Une fois connecté, allez dans la section 
"Configurer mon routeur".

Pour plus d’informations, vous pouvez consulter 
[cette page https://craym.eu/tutoriels/utilitaires/ouvrir_les_ports_de_sa_box.html].



===Un nom de domaine===[NDD] 

Vous voudrez certainement obtenir un nom de domaine, qui permettra à tous
d'accéder plus facilement à votre serveur. Cela vous donne aussi la possibilité de
mieux vous organiser avec des sous-domaines, par exemple mail.&NDD,
blog.&NDD...

	Mais c'est quoi un nom de domaine?

Imaginons que M. Ali Gator vit au 5 rue du moulin à Picsouville. Pour aller lui
rendre visite, c’est à cette adresse que vous allez vous rendre.
Sur le web, l’adresse de votre serveur, c’est une série de nombres : l'[IP #IP].
Par exemple &IPV4. C’est pratique pour les machines, pas pour les
humains.

Un nom de domaine nous permet d’utiliser l’adresse wikipedia.org, 
qui est traduit par les ordinateurs en 91.198.174.192.
Avouez que c’est plus facile à retenir.

L’association d’une IP avec un nom de domaine est possible grâce aux
enregistrements DNS (Domain Name System). Ce système est à la base de toutes les
interactions qui se servent des noms de domaines : Email, Web, ... C'est une
sorte d'annuaire numérique.

Vous pouvez acheter un nom de domaine auprès de ce que l’on appelle
un registre ou registrar comme [OVH https://www.ovh.com/fr] ou 
[Gandi http://gandi.net]. Ce n'est pas très onéreux, mais sachez qu'il 
en existe aussi des gratuits, comme ceux de 
[FDN http://www.fdn.fr] ou
[freenom.com http://www.freenom.com/fr/index.html?lang=fr].

Une fois votre domaine acquis, configurez les champs DNS pour le faire
pointer vers votre adresse IP. Pour ça, référez vous à la partie
suivante sur les [registres #registre]. Si vous êtes un aventurier, vous voudrez
peut-être penser à gérer les enregistrements DNS vous-mêmes en installant un
[serveur de noms #serveur-de-noms].



===Les enregistrements DNS===[registre]
Lorsque un ordinateur doit aller sur un site dont le nom est
"&NDD", il va demander à un serveur DNS (qu'on appelle
résolveur dans ce cas) à quelle adresse IP ce nom de domaine correspond. Le DNS, c'est un
peu comme des panneaux sur le bord de la route qui indiquent le chemin à suivre
pour arriver à destination.

Pour créer ce panneau une fois que vous avez un nom de domaine, il faut le
relier à l’adresse IP de votre serveur. Mais si, souvenez-vous, cette série de
nombres ressemblant à ``&IPV4``.  Pour cela, enregistrez un champ de
type A dans l’interface d’administration du registre. Par exemple :

```
&NDD	A	&IPV4
```


Notez qu'il existe plusieurs types d’enregistrements :
- Les champs A pour les adresses IPv4. ex : ``&IPV4``.
- Les champs AAAA pour les adresses IPv6. ex: ``&IPV6``.
- Les champs CNAME. Ils sont très utiles pour définir des sous-domaines et
  organiser votre serveur. Vous aurez alors plusieurs CNAME qui pointeront vers
  le champ A précédent. Par exemple : 
  
```
blog.&NDD CNAME &NDD
wiki.&NDD CNAME &NDD
webmail.&NDD CNAME &NDD
```
- Les champs MX, NS ,TXT.


Pour en savoir plus, vous pouvez lire les pages suivantes : 

- [Page Wikipédia https://fr.wikipedia.org/wiki/Domain_Name_System]
  sur le DNS;
- Site interactif pour comprendre le [fonctionnement https://howdns.works/] de
  DNS.





