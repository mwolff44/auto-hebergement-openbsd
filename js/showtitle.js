var timeout_title;
var headers = document.querySelectorAll("h1, h2, h3, h4, h5, h6");

window.addEventListener('scroll', function(){
	clearTimeout(timeout_title);
	timeout_title = setTimeout(function(){
		for (var i =0; i < headers.length; i++) {
			if ( document.body.scrollTop + 50 >= headers[i].offsetTop ) {
				document.getElementsByClassName("isread")[0].innerHTML =
                    '▼ ' +
					headers[i].innerHTML +
                    ' ▼';
			}
		}
	}, 100);
});


