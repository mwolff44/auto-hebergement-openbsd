Auto-hébergement facile avec OpenBSD
====================================

Ce dépôt git permet à chacun de contribuer à l'amélioration de ce document.

Vous pouvez consulter le rendu 
[en suivant ce lien](http://yeuxdelibad.net/ah/dev) (généré périodiquement).

Afin de participer, vous pouvez : 

- Ouvrir une nouvelle Issue,
- Cloner le dépôt et proposer des modifications : 
    1. Forker avec git clone
    2. Créer une branche : git checkout -b votremodif
    3. Faire vos changements : ``git add``, ``git commit``. N'oubliez
       pas de vous citer comme auteur dans l'en-tête du fichier
       modifié/ajouté.
    4. Demander à intégrer les changements (merge request)

Assurez-vous de travailler sur une version à jour avant de demander
l'intégration de vos changements : 

    git remote add https://framagit.org/Thuban/auto-hebergement-openbsd.git
    git fetch upstream
    git pull origin master
    git pull upstream master


Rédaction
---------
La rédaction se fait au format [txt2tags](http://txt2tags.org/markup.html).

Quelques conventions d'écriture : 
- Rester simple !
- Utiliser les variables suivantes : 
    - &NDD : nom de domaine "chezmoi.tld" à titre d'exemple.
    - &IPV4 : 192.0.2.2 (192.0.2.0/24 pour les adresses publiques (reseau reservé pour la
documentation))
    - &IPV6 : 2001:db8::2 et fd19:6f4d:bea8:f63a::2
- Tester.


