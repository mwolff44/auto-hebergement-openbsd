Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8

==Seedbox==
Ce chapitre décrit comment mettre en place une 
[seedbox https://fr.wikipedia.org/wiki/Seedbox].

On commence par installer l'excellent rtorrent : 
```
# pkg_add rtorrent
```

On ajoute maintenant un utilisateur dont l'unique tâche sera de faire tourner rtorrent : 


```
# adduser
Use option ``-silent'' if you don't want to see all warnings and questions.

Reading /etc/shells
Check /etc/master.passwd
Check /etc/group

Ok, let's go.
Don't worry about mistakes. There will be a chance later to correct any input.
Enter username []: _rtorrent
Enter full name []: rtorrent daemon
Enter shell csh ksh nologin sh [nologin]: ksh
Uid [1005]:
Login group _rtorrent [_rtorrent]:
Login group is ``_rtorrent''. Invite _rtorrent into other groups: guest no
[no]:
Login class authpf bgpd daemon default dovecot pbuild staff unbound
[default]:
Enter password []:
Enter password again []:

Name:        _rtorrent
Password:    ***************
Fullname:    rtorrent daemon
Uid:         1005
Gid:         1005 (_rtorrent)
Groups:      _rtorrent
Login Class: default
HOME:        /home/_rtorrent
Shell:       /sbin/ksh
OK? (y/n) [y]: y
Added user ``_rtorrent''
Add another user? (y/n) [y]: n
Goodbye!
```

Nous pouvons maintenant nous connecter en tant qu'utilisateur _rtorrent : 

```
# su _rtorrent
```

Nous allons créer les dossiers qui serviront à télécharger les torrents, ainsi
qu'un dossier dans lequel tous les fichiers .torrent ajoutés seront directement
pris en charge par rtorrent : 

```
$ mkdir -p Telechargements/{download,session,torrents}
```

On crée maintenant le fichier ``~/.rtorrent.rc``. Vous pouvez copier l'exemple
fourni avec le paquet : 

```
$ cp /usr/local/share/examples/rtorrent ~/.rtorrent.rc
```

On modifie la configuration selon nos besoins : 

```
# Maximum and minimum number of peers to connect to per torrent.
#min_peers = 40
#max_peers = 100

# Same as above but for seeding completed torrents (-1 = same as downloading)
#min_peers_seed = 10
#max_peers_seed = 50

# Maximum number of simultanious uploads per torrent.
#max_uploads = 15

# Global upload and download rate in KiB. "0" for unlimited.
#download_rate = 0
upload_rate = 20

# Default directory to save the downloaded torrents.
directory = ~/Telechargements/download 

# Default session directory. Make sure you don't run multiple instance
# of rtorrent using the same session directory. Perhaps using a
# relative path?
session = ~/Telechargements/session

# Watch a directory for new torrents, and stop those that have been
# deleted.
schedule = watch_directory,5,5,load_start=~/Telechargements/torrents/*.torrent
schedule = untied_directory,5,5,stop_untied=

# Close torrents when diskspace is low.
#schedule = low_diskspace,5,60,close_low_diskspace=100M

# The ip address reported to the tracker.
#ip = 127.0.0.1
#ip = rakshasa.no

# The ip address the listening socket and outgoing connections is
# bound to.
#bind = 127.0.0.1
#bind = rakshasa.no

# Port range to use for listening.
#port_range = 6890-6999

# Start opening ports at a random position within the port range.
#port_random = no

# Check hash for finished torrents. Might be usefull until the bug is
# fixed that causes lack of diskspace not to be properly reported.
#check_hash = no

# Set whether the client should try to connect to UDP trackers.
#use_udp_trackers = yes

# Alternative calls to bind and ip that should handle dynamic ip's.
#schedule = ip_tick,0,1800,ip=rakshasa
#schedule = bind_tick,0,1800,bind=rakshasa

# Encryption options, set to none (default) or any combination of the following:
# allow_incoming, try_outgoing, require, require_RC4, enable_retry, prefer_plaintext
#
# The example value allows incoming encrypted connections, starts unencrypted
# outgoing connections but retries with encryption if they fail, preferring
# plaintext to RC4 encryption after the encrypted handshake
#
encryption = allow_incoming,require,require_rc4,enable_retry

# Enable DHT support for trackerless torrents or when all trackers are down.
# May be set to "disable" (completely disable DHT), "off" (do not start DHT),
# "auto" (start and stop DHT as needed), or "on" (start DHT immediately).
# The default is "off". For DHT to work, a session directory must be defined.
# 
dht = auto

# UDP port to use for DHT. 
# 
# dht_port = 6881

# Enable peer exchange (for torrents not marked private)
#
# peer_exchange = yes

system.method.set_key = event.download.finished,notify_me,"execute=~/.rtorrent_mail.sh,$d.get_name="

#scgi_port = 127.0.0.1:5000
```

Afin d'être averti lorsqu'un téléchargement est terminé, on crée le script
``~/.rtorrent_mail.sh`` : 

```
#!/bin/sh
echo "$(date) : $1 - Download completed." | mail -s "[rtorrent] - Download completed : $1" root
```

Notez que tous les fichiers .torrents qui seront déposés dans le dossier
``watch`` seront directement ajoutés dans rtorrent. Si vous voulez
ajouter un fichier torrent à partir de votre ordinateur habituel, vous pouvez
utiliser un client [SFTP #sftp] ou la commande scp : 

```
$ scp -p <port_ssh> _rtorrent@&NDD:/home/_rtorrent/Telechargements/watch
```

Reste à lancer rtorrent en arrière-plan à chaque démarrage du serveur.
Revenez sur le compte root (ctrl-D) puis ajoutez la commande suivante dans le
fichier ``/etc/rc.local`` : 

```
/usr/bin/su _rtorrent -c "/usr/bin/tmux new -s rtorrent -d /usr/local/bin/rtorrent"

```

On utilise l'excellent [tmux https://tmux.github.io/] installé par défaut sur OpenBSD
pour envoyer rtorrent en arrière-plan. 

Si vous souhaitez administrer rtorrent par la suite, connectez-vous en ssh avec
l'utilisateur _rtorrent, et affichez rtorrent avec la commande 
``tmux a -t rtorrent``. Appuyez successivement sur "ctrl-b" puis la touche ``d``
pour vous détacher de rtorrent.
